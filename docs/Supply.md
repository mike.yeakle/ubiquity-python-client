# Supply


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maximum** | **str** | Maximum supply | [optional] 
**total** | **str** | Total supply at block height, excluding burnt coins | [optional] 
**total_created** | **str** | Total coins created historically up until this block | [optional] 
**total_burnt** | **str** | Total coins burnt historically up until this block | [optional] 
**created** | **str** | Coins created at this block | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


