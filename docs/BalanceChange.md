# BalanceChange

Change of balance of a currency

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**old** | **str** | Balance before transaction | [optional] 
**delta** | **str** | Balance difference | [optional] 
**new** | **str** | Balance after transaction | [optional] 
**currency** | [**Currency**](Currency.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


